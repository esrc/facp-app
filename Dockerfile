FROM python:3.8-buster

RUN apt-get update && apt-get install -y build-essential libsasl2-dev python-dev libldap2-dev libssl-dev
RUN pip install requests pyramid pyramid_mako pyramid_beaker pyramid_debugtoolbar Paste \
    pyramid_simpleform pyramid_tm python_ldap webhelpers lxml nose \
    nose-progressive rednose nose_fixes waitress cookiecutter configparser

